#!/bin/bash

usage() {
  cat <<- EOF >&2
	Usage: $0 param1 param2

	  * param1 - first file with expression
	  * param2 - second file with expression
	EOF
  exit
}

[ -z "$1" ] && usage || true

if [[ -n "$1" && -z "$2" ]]
then
  var1=$(cat $1)
  echo $(($var1))
fi

if [[ -n "$2" ]]
then
  var1=$(cat $1)
  var2=$(cat $2)
  result1=$(($var1))
  result2=$(($var2))

  if [[ "$result1" -le "$result2" ]]
  then
    echo $result2
  fi

  if [[ "$result1" -gt "$result2" ]]
  then
    echo $result1
  fi
fi
